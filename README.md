# Inhaltsverzeichniss

## Dokumentation

[Anforderungen Analysieren](dokumentation/Anforderungen%20analysieren/README.md)

[Planen Entscheiden](dokumentation/Planen%20und%20Entscheiden/Planen.md)

[Entwicklung microservices](dokumentation/Entwicklung%20der%20Microservices/Entwicklung.md)

[Webapplikation](dokumentation/Web-Applikation%20und%20APIs%20entwickeln/Web-Applikation.md)

[Speicherung der Daten](dokumentation/Persistente%20speicherung%20der%20Daten/Datenbank.md)

[Skalierung der Webapplikation](dokumentation/Skalierung%20der%20Web%20Applikation/Skalierung.md)

[Monitoring](dokumentation/Monitoring/Monitoring.md)

[Image Registry](dokumentation/Image%20Registry/Image%20Registry.md)

[Datensicherheit](dokumentation/Datensicherheit%20implementieren/Datensicherheit.md)

## Troubleshooting

[Troubleshooting](dokumentation/Troubleshooting/Datenbank.md)

## Code

[Alle Codes](dokumentation/Dokumentation/)

