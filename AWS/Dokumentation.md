# Anforderungen analysieren

Für die Anforderungsanalyse einer Webseite, auf der Benutzer Daten eingeben und in einer Datenbank speichern können, mit einer Containerumgebung auf AWS inklusive Load Balancer.


Ich muss meine Images in die AWS Cloud hochladen.
Ich muss Aufgaben ausführen, um die Container zu starten.


# Planen und entscheiden


* Schritt 1: Planen
    * Woher bekomme ich die Infos
    * Was braucheich .
* Schritt 2: AWS Selbststudium
    * Wie übertrage ich ein Image
    * Was brauche ich alles damit ein Container lauft
* Schritt 3: Images Pushen
    * Alle images Auf AWS Pushen
* Schritt 4: Container Starten
    * Die Container zum laufen bringen, versuchen zu starten.
* Schritt 5: Dokumentieren
    * Erstellung einer ausführlichen Dokumentation des gesamten Prozesses
    * Beschreibung der einzelnen Container und deren Konfigurationen




![alt text](image-1.png)
# Infrastruktur für die Microservices bereitstellen

Ich werde meine Infrastruktur auf AWS bereitstellen.

# Entwicklung der Microservices

Für die Entwicklung und Bereitstellung der Microservices in einer flexiblen und skalierbaren Infrastruktur auf AWS sind mehrere Schritte notwendig. Die Architektur umfasst einen NGINX-Webserver, der über einen Load Balancer auf die Backend-Services zugreift, die in Containern ausgeführt werden. Der Backend-Service nutzt Express.js für die API-Kommunikation mit einer MySQL-Datenbank. Prometheus und Grafana werden zur Überwachung und Visualisierung der Systemleistung integriert. Die Konfigurationen und Abhängigkeiten sind in einem Docker-Compose-File festgelegt, das die Deployment-Architektur klar abbildet.

Die NGINX-Konfiguration leitet Anfragen an den Backend-Service weiter, der über eine Docker-Netzwerkkonfiguration mit MySQL verbunden ist. Grafana verwendet PromQL-Abfragen über einen spezifischen Exporter, um Metriken zu visualisieren und die Systemleistung zu überwachen. Die gesamte Infrastruktur ist auf Skalierbarkeit und Sicherheit ausgelegt, wobei AWS-Dienste wie ECS für die Container-Orchestrierung und RDS für die Datenbankverwaltung genutzt werden.

Dieser Aufbau ermöglicht eine effiziente Entwicklung und Bereitstellung von Microservices in einer Cloud-Umgebung, optimiert für E-Commerce-Anwendungen mit robusten Monitoring-Funktionen.

```yaml
version: '3.7'

services:
  mysql:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD: example
      MYSQL_DATABASE: meinedatenbank
    volumes:
      - mysql-data:/var/lib/mysql

  backend:
    build: ./backend
    ports:
      - "3000:3000"
    depends_on:
      - mysql

  nginx:
    image: nginx:alpine
    ports:
      - "8080:80"
    volumes:
      - ./html:/usr/share/nginx/html
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - backend

  prometheus:
    image: prom/prometheus
    volumes:
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - "9090:9090"

  grafana:
    image: grafana/grafana
    ports:
      - "3001:3000"
    depends_on:
      - prometheus
    volumes:
      - grafana-data:/var/lib/grafana

  nginx-exporter:
    image: nginx/nginx-prometheus-exporter
    ports:
      - 9113:9113
    command: ["-nginx.scrape-uri", "http://nginx:8080/nginx_status"]

volumes:
  mysql-data:
  grafana-data:

networks:
  default:
    external:
      name: bridge
```

Die Container-Konfiguration für den Backend-Service:

```Dockerfile
FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "server.js"]
```

Diese Architektur stellt sicher, dass die Microservices effizient entwickelt, bereitgestellt und überwacht werden können, um den Anforderungen eines E-Commerce-Systems gerecht zu werden.



# Web-Applikation und APIs entwickeln

Eine benutzerfreundliche Web-Applikation besteht aus Frontend und Backend Microservices, gehostet auf AWS in Containern. Nginx fungiert als Reverse Proxy für die Webanwendung und die RESTful APIs, die in Node.js mit Express entwickelt sind. Daten werden in einer MySQL-Datenbank gespeichert. Die AWS-Infrastruktur umfasst Docker-Container für MySQL, Nginx, sowie Prometheus und Grafana für Monitoring.

# Persistente Speicherung der Daten

Um die persistente Speicherung von Geschäftsdaten in Ihrer Microservices-Architektur sicherzustellen, integrieren Sie eine MySQL-Datenbank und Volumes für dauerhafte Datenhaltung. Auf AWS können Sie die MySQL-Datenbank in einem Docker-Container mit persistentem Speicher über Amazon RDS oder als eigenständigen Container mit einem Docker Volume konfigurieren. Die Datenbank wird über einen eigenen Service gestartet und durch den Backend-Service angesprochen, der die Verbindung zur Datenbank 

# Selbststudium

ich habe das meiste über AWS mit Youtubevideos gelernt. 

wie beispielsweise wie man ein container startet



# security-group

![alt text](image-7.png)

# Loadbalancer-Skalierung


![alt text](image-2.png)


# Cluster

![alt text](image-6.png)

# Images

Nginxyur ist mein eigenes image von dem Containerregistry

![alt text](image-3.png)

# Nginx
![alt text](image-4.png)

# Datenbank

Amazon RDS (Relational Database Service) ist ein verwalteter Dienst von AWS für relationale Datenbanken. Es bietet automatisierte Wartung, einfache Skalierbarkeit, Datenverschlüsselung, Multi-AZ-Bereitstellungen für hohe Verfügbarkeit und flexible Preismodelle. RDS ermöglicht es, sich auf Anwendungs- statt auf Infrastrukturmanagement zu konzentrieren.

![alt text](image-5.png)

