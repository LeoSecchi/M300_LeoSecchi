const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql2');

const app = express();
const port = 3000;

// Middleware für das Parsen von JSON-Daten
app.use(bodyParser.json());

// Verbindung zur MySQL-Datenbank
const db = mysql.createConnection({
    host: 'mysql',
    user: 'root',
    password: 'example',
    database: 'meinedatenbank'
});

// Verbindung zur Datenbank herstellen
db.connect(err => {
    if (err) {
        console.error('Fehler bei der Verbindung zur Datenbank:', err);
        return;
    }
    console.log('Verbunden mit der Datenbank');
    // Tabelle erstellen, wenn sie nicht existiert
    db.query('CREATE TABLE IF NOT EXISTS inputs (id INT AUTO_INCREMENT PRIMARY KEY, text VARCHAR(255) NOT NULL)', (err, result) => {
        if (err) {
            console.error('Fehler beim Erstellen der Tabelle:', err);
            return;
        }
        console.log('Tabelle erfolgreich erstellt');
    });
});

// Route für das Speichern von Daten
app.post('/api/save', (req, res) => {
    console.log('Empfangene Anfrage:', req.body);
    const inputText = req.body.input;
    const query = 'INSERT INTO inputs (text) VALUES (?)';
    db.query(query, [inputText], (err, result) => {
        if (err) {
            console.error('Fehler beim Speichern:', err);
            res.status(500).json({ message: 'Fehler beim Speichern' });
            return;
        }
        res.json({ message: 'Erfolgreich gespeichert' });
    });
});


// Server starten und auf Verbindungen auf dem angegebenen Port warten
app.listen(port, () => {
    console.log(`Server läuft auf Port ${port}`);
});
