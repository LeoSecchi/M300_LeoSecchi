# Entwicklung



#### Architekturüberblick

Containerumgebung

Unsere Containerumgebung besteht aus mehreren Services, die jeweils einen spezifischen Microservice repräsentieren:

* MySQL-Service: 
    * Dieser Service verwaltet die persistente Datenbank für die Anwendung. Durch die Verwendung eines separaten Containers können wir die Datenbank von anderen Diensten isolieren und eine klare Trennung von Daten und Logik gewährleisten.

* Backend-Service: 
    * Der Backend-Service implementiert die Geschäftslogik und stellt die Schnittstellen für die Clientanwendungen bereit. Er ist in Node.js geschrieben und verwendet Express.js für die Erstellung von RESTful-APIs. Durch die Containerisierung können wir den Backend-Service unabhängig von anderen Komponenten entwickeln, testen und bereitstellen.

* Nginx-Service: 
    * Als Reverse-Proxy dient Nginx zur Bereitstellung von statischen Inhalten und zur Weiterleitung von Anfragen an den Backend-Service. Durch die Verwendung eines eigenen Containers für Nginx können wir die Konfiguration flexibel anpassen und die Skalierbarkeit verbessern.

* Prometheus-Service: 
    * Prometheus wird verwendet, um Metriken und Leistungsdaten der Containerumgebung zu sammeln und zu überwachen. Durch die Containerisierung von Prometheus können wir das Monitoring-Tool einfach integrieren und zentralisiert verwalten.

 * Grafana-Service: 
      * Grafana bietet ein benutzerfreundliches Dashboard zur Visualisierung und Analyse der von Prometheus gesammelten Metriken. Durch die Containerisierung von Grafana können wir das Monitoring und die Analyse der Containerinfrastruktur effizient durchführen.