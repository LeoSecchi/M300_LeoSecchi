# M300 LeoSecchi

Anforderungen analysieren <br>
Geschäftsanforderungen und die daraus resultierende Microservices der E-Commerce Lösung identifizieren

# Projektidee


Ich plane eine Webseite zu erstellen, um herauszufinden, wie viele Menschen den Künstler Kanye West auf dem oben gezeigten Bild erkennen. Die Webseite wird ein Bild von Kanye West anzeigen und darunter ein Eingabefeld haben, in dem Benutzer den Künstler identifizieren können. Die eingegebenen Antworten werden in einer Datenbank gespeichert und können später eingesehen werden, um zu sehen, wie viele Personen den Künstler richtig erkannt haben.