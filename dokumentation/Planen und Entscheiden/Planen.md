# Planen

Hier ist meine planung von diesem Projekt.

# Zeitplan

Hier ist mein Gantt-Diagramm. Ich habe mein Projekt in fünf Schritte unterteilt: Planen, Container starten, Container verknüpfen, Troubleshooting und Dokumentieren.

* Schritt 1: Planen
    * Identifizierung der notwendigen Services (MySQL, Backend, Nginx, Prometheus, Grafana)
    * Erstellen einer docker-compose.yml Datei
    * Definition der Netzwerke und Volumes
* Schritt 2: Container starten
    * Starten der Container mit Docker Compose
    * Sicherstellen, dass alle Container korrekt gestartet sind und laufen
* Schritt 3: Container verknüpfen
    * Überprüfen der Konnektivität zwischen den Containern
    * Testen der Abhängigkeiten (z.B. Backend muss auf MySQL zugreifen können)
    * Konfiguration der Prometheus- und Grafana-Container zur Überwachung der anderen Container
* Schritt 4: Troubleshooting
    * Fehlerbehebung bei nicht startenden oder nicht funktionierenden Containern
    * Überprüfung der Logfiles und Anpassung der Konfigurationen
    * Sicherstellen, dass alle Container wie gewünscht miteinander kommunizieren
* Schritt 5: Dokumentieren
    * Erstellung einer ausführlichen Dokumentation des gesamten Prozesses
    * Beschreibung der einzelnen Container und deren Konfigurationen

<br>

![d](../Bilder/Screenshot%202024-06-09%20203329.png)
<br>

# Microservices

Hier sind die Microservices, für die ich mich entschieden habe, zusammen mit den Gründen für meine Auswahl:

* Webserver: Nginx

    *  Gründe: Nginx ist bekannt für seine hohe Leistungsfähigkeit und Flexibilität. Es kann eine große Anzahl von gleichzeitigen Verbindungen verarbeiten und eignet sich hervorragend für den Einsatz in modernen Webanwendungen. Außerdem bietet Nginx vielseitige Funktionen wie Load Balancing, Reverse Proxying und Caching, was es zu einer idealen Wahl für den Webserver in unserer Microservices-Architektur macht.

* Datenbank: MySQL

    * Gründe: MySQL ist eine weit verbreitete relationale Datenbank, die sich durch ihre Zuverlässigkeit und Stabilität auszeichnet. Sie bietet umfangreiche Funktionen und eine starke Community-Unterstützung. MySQL ist auch gut dokumentiert und kompatibel mit vielen Anwendungen und Frameworks, was die Integration und Verwaltung erleichtert. Diese Eigenschaften machen MySQL zu einer optimalen Wahl für die Datenbankkomponente unserer Anwendung.

* Monitoring-Tool: Prometheus

    * Gründe: Prometheus ist ein leistungsfähiges und flexibles Monitoring- und Alarmsystem, das speziell für die Überwachung von Microservices-Architekturen entwickelt wurde. Es ermöglicht die Sammlung und Auswertung von Metriken in Echtzeit, was für die proaktive Überwachung und Fehlersuche unerlässlich ist. Prometheus bietet eine starke Integration mit anderen Tools und eine benutzerfreundliche Query-Sprache (PromQL), was es zu einem wertvollen Werkzeug für die Überwachung und Optimierung unserer Infrastruktur macht.

# Netzwerkplan

![d](../Bilder/netzwerk.png)