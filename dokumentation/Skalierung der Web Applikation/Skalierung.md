### Skalierung der Web-Applikation: Horizontale und Vertikale Skalierung

Um die Leistungsfähigkeit und Verfügbarkeit unserer Web-Applikation sicherzustellen, implementieren wir sowohl horizontale als auch vertikale Skalierungstechniken. Diese ermöglichen es uns, auf steigende Benutzerzahlen und Lastspitzen effektiv zu reagieren und eine optimale Benutzererfahrung zu gewährleisten.

#### Horizontale Skalierung

Die horizontale Skalierung bezieht sich auf das Hinzufügen zusätzlicher Instanzen unserer Anwendung, um die Last auf mehrere Maschinen oder Container zu verteilen. In unserer Containerumgebung wird die horizontale Skalierung durch Docker Compose und die Containerorchestrierung ermöglicht. Durch das einfache Hinzufügen weiterer Instanzen des Backend-Services können wir die Last gleichmäßig auf mehrere Container verteilen.

Um die horizontale Skalierung zu implementieren, können wir das `docker-compose.yml`-File anpassen, um die Anzahl der Instanzen des Backend-Services zu erhöhen. Beispielsweise können wir die Anzahl der Replikate auf zwei oder mehr erhöhen, um die Last zu verteilen:

```yaml
backend:
  build: ./backend
  ports:
    - "3000:3000"
  depends_on:
    - mysql
  # Hier kann die Anzahl der Replikate konfiguriert werden
  scale: 2
```

Durch die horizontale Skalierung können wir die Leistungsfähigkeit, meiner Anwendung erhöhen, Ausfallsicherheit verbessern und Lastspitzen bewältigen, ohne die Leistung zu beeinträchtigen.

