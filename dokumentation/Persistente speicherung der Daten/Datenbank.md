### Persistente Speicherung der Daten und Integration einer Datenbank

Um eine zuverlässige und skalierbare Datenhaltung für das Webportal zu gewährleisten, wurde eine Datenbank in die Containerumgebung integriert. Hierbei wird die MySQL-Datenbank verwendet, die als eigenständiger Service innerhalb der Containerumgebung läuft.

#### MySQL-Datenbank-Service

Die MySQL-Datenbank wird als eigenständiger Service innerhalb der Containerumgebung bereitgestellt. Dabei wird das offizielle MySQL-Image verwendet, um eine konsistente und zuverlässige Datenbankinstanz zu gewährleisten. Die Konfiguration erfolgt über Umgebungsvariablen, wodurch der Root-Benutzername, das Root-Passwort und der Name der Datenbank definiert werden können.

Die Datenbank ist persistent, da die Daten in einem separaten Volume (/var/lib/mysql) gespeichert werden. Dadurch bleiben die Daten auch nach dem Neustart oder der Aktualisierung des Containers erhalten.


#### Vorteile

Die Integration einer persistente Datenbank bietet mehrere Vorteile:

- **Dauerhafte Speicherung**: Daten werden dauerhaft in der MySQL-Datenbank gespeichert, auch wenn Container neu gestartet oder aktualisiert werden.

- **Zuverlässigkeit**: Die Verwendung einer Datenbank verbessert die Zuverlässigkeit des Systems, da Daten konsistent gespeichert und abgerufen werden können.

- **Skalierbarkeit**: Die Containerinfrastruktur ermöglicht es, die Datenbankressourcen je nach Bedarf zu skalieren, um steigende Anforderungen zu bewältigen.

Durch die Integration einer persistenten Datenbank wird eine solide Grundlage für das Webportal geschaffen, die eine effiziente Datenhaltung und -verwaltung gewährleistet.