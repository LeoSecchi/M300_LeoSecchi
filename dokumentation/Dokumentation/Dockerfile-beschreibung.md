```yaml

# Dockerfile für die Containerumgebung

# Verwenden des offiziellen Node.js 14-Images als Basis
FROM node:14

# Festlegen des Arbeitsverzeichnisses innerhalb des Containers
WORKDIR /usr/src/app

# Kopieren der package.json-Datei in das Arbeitsverzeichnis
COPY package*.json ./

# Ausführen von npm install, um Abhängigkeiten zu installieren
RUN npm install

# Kopieren des gesamten Projektinhalts in das Arbeitsverzeichnis des Containers
COPY . .

# Freigabe des Ports 3000 für die Kommunikation mit dem Container
EXPOSE 3000

# Definieren des Befehls, der beim Start des Containers ausgeführt wird
CMD ["node", "server.js"]
