```yaml
# prometheus/prometheus.yml
global:
  scrape_interval: 15s

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['prometheus:9090']
```

Das `prometheus.yml`-Konfigurationsfile definiert die Konfiguration für den Prometheus-Monitoring-Dienst in unserer Containerumgebung. Hier ist eine Erklärung der einzelnen Abschnitte:

- `global`: Hier werden globale Konfigurationsoptionen festgelegt, die für alle Scrapes gelten. In diesem Fall ist `scrape_interval` auf 15 Sekunden festgelegt, was bedeutet, dass Prometheus alle 15 Sekunden Metriken von den konfigurierten Zielen abruft.

- `scrape_configs`: Dieser Abschnitt definiert die Konfiguration für die Scraping-Jobs, die Prometheus ausführen soll. Im Beispiel ist ein Job mit dem Namen "prometheus" definiert. Dieser Job konfiguriert statische Zielen (`static_configs`), die Prometheus abfragen soll. Hier ist das Ziel `prometheus:9090` angegeben, was bedeutet, dass Prometheus seine eigenen Metriken über den Port 9090 abrufen wird.

Dieses Konfigurationsfile ermöglicht es Prometheus, Metriken von den konfigurierten Zielen zu sammeln, was für das Monitoring und die Analyse der Containerinfrastruktur von entscheidender Bedeutung ist.