```yaml
version: '3.7'

services:
  # MySQL Service
  mysql:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD: example
      MYSQL_DATABASE: meinedatenbank
    volumes:
      - mysql-data:/var/lib/mysql

  # Backend Service
  backend:
    build: ./backend
    ports:
      - "3000:3000"
    depends_on:
      - mysql

  # Nginx Service
  nginx:
    image: nginx:alpine
    ports:
      - "8080:80"
    volumes:
      - ./html:/usr/share/nginx/html
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - backend

  # Prometheus Service
  prometheus:
    image: prom/prometheus
    volumes:
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - "9090:9090"
    networks:
      - my_network

  # Grafana Service
  grafana:
    image: grafana/grafana
    platform: linux/amd64
    ports:
      - "3001:3000"
    depends_on:
      - prometheus
    networks:
      - my_network
    volumes:
      - grafana-data:/var/lib/grafana

volumes:
  mysql-data:
  grafana-data:

networks:
  my_network:
```

Dieses Docker Compose File definiert die Konfigurationen für die Containerumgebung. Es besteht aus verschiedenen Services, die die einzelnen Komponenten der Infrastruktur repräsentieren:

- **MySQL**: Ein Service für die MySQL-Datenbank.
- **Backend**: Der Service für den Backend-Server, der auf Port 3000 läuft.
- **Nginx**: Ein Service für den Nginx-Server, der als Reverse-Proxy für den Backend-Service dient und statische Inhalte bereitstellt.
- **Prometheus**: Der Service für das Prometheus-Monitoring-Tool.
- **Grafana**: Der Service für das Grafana-Dashboard zur Visualisierung der Metriken von Prometheus.

Die Dienste sind untereinander verbunden und kommunizieren über ein gemeinsames Netzwerk (`my_network`). Volumes werden verwendet, um persistente Daten für die MySQL-Datenbank und Grafana zu speichern.

Die `depends_on`-Direktive definiert Abhängigkeiten zwischen den Diensten, um sicherzustellen, dass sie in der richtigen Reihenfolge gestartet werden.