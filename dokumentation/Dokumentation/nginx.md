```nginx
server {
    listen 80;

    server_name localhost;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    location /api/ {
        proxy_pass http://backend:3000/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Die `nginx.conf` konfiguriert den Nginx-Server, der als Reverse-Proxy fungiert, um Anfragen an den Backend-Service weiterzuleiten und statische Inhalte zu servieren. Hier ist eine Erklärung der Konfiguration:

- `listen 80;`: Gibt an, dass Nginx auf Port 80 lauscht, dem Standard-HTTP-Port.
  
- `server_name localhost;`: Definiert den Servernamen als "localhost", was bedeutet, dass dieser Server auf Anfragen antwortet, die an "localhost" gerichtet sind.
  
- `location /`: Legt die Konfiguration für Anfragen fest, die an die Haupt-URL gerichtet sind. Hier wird der Pfad `/` definiert, der alle Anfragen abdeckt.
  
- `root /usr/share/nginx/html;`: Legt das Wurzelverzeichnis für statische Dateien fest. In diesem Fall werden die statischen Dateien im Verzeichnis `/usr/share/nginx/html` erwartet.
  
- `index index.html index.htm;`: Gibt die Standarddateien an, die verwendet werden sollen, wenn keine spezifische Datei angefordert wird. In diesem Fall werden `index.html` oder `index.htm` verwendet.
  
- `location /api/`: Definiert eine weitere Konfiguration für Anfragen, die mit `/api/` beginnen.
  
- `proxy_pass http://backend:3000/;`: Leitet Anfragen, die mit `/api/` beginnen, an den Backend-Service weiter, der unter dem Namen "backend" auf Port 3000 erreichbar ist.
  
- Die nachfolgenden `proxy_set_header`-Anweisungen setzen verschiedene Header, die Informationen über die ursprüngliche Anfrage enthalten, wie Host, IP-Adresse und Protokoll.

Diese Konfiguration ermöglicht es Nginx, eingehende Anfragen zu verarbeiten und sie je nach Pfad an den entsprechenden Dienst weiterzuleiten, wodurch eine saubere Trennung von statischen Inhalten und dynamischen Inhalten erreicht wird.