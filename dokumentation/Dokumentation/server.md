```js
/*
Der server.js ist das Hauptskript für unseren Backend-Service, der auf einem Node.js-Server läuft. Hier wird die Logik für die API-Routen definiert und die Verbindung zur MySQL-Datenbank hergestellt.
*/

// Erforderliche Module importieren
const express = require('express'); // Express-Framework für die Erstellung von RESTful-APIs
const bodyParser = require('body-parser'); // Middleware für das Parsen von JSON-Daten
const mysql = require('mysql2'); // MySQL-Modul für die Datenbankverbindung

// Express-App initialisieren
const app = express();
const port = 3000; // Der Port, auf dem der Server lauscht

// Middleware für das Parsen von JSON-Daten registrieren
app.use(bodyParser.json());

// Verbindung zur MySQL-Datenbank herstellen
const db = mysql.createConnection({
    host: 'mysql', // Der Hostname des MySQL-Containers (aufgrund des Docker-Netzwerks)
    user: 'root', // Der Benutzername für die MySQL-Datenbank
    password: 'example', // Das Passwort für die MySQL-Datenbank
    database: 'meinedatenbank' // Der Name der MySQL-Datenbank
});

// Verbindung zur Datenbank herstellen
db.connect(err => {
    if (err) {
        console.error('Fehler bei der Verbindung zur Datenbank:', err);
        return;
    }
    console.log('Verbunden mit der Datenbank');
    // Tabelle erstellen, wenn sie nicht existiert
    db.query('CREATE TABLE IF NOT EXISTS inputs (id INT AUTO_INCREMENT PRIMARY KEY, text VARCHAR(255) NOT NULL)', (err, result) => {
        if (err) {
            console.error('Fehler beim Erstellen der Tabelle:', err);
            return;
        }
        console.log('Tabelle erfolgreich erstellt');
    });
});

// Route für das Speichern von Daten definieren
app.post('/api/save', (req, res) => {
    console.log('Empfangene Anfrage:', req.body);
    const inputText = req.body.input;
    const query = 'INSERT INTO inputs (text) VALUES (?)'; // SQL-Abfrage zum Einfügen von Daten
    db.query(query, [inputText], (err, result) => {
        if (err) {
            console.error('Fehler beim Speichern:', err);
            res.status(500).json({ message: 'Fehler beim Speichern' }); // Fehlermeldung zurückgeben
            return;
        }
        res.json({ message: 'Erfolgreich gespeichert' }); // Erfolgsmeldung zurückgeben
    });
});

// Server starten und auf Verbindungen auf dem angegebenen Port warten
app.listen(port, () => {
    console.log(`Server läuft auf Port ${port}`);
});

```